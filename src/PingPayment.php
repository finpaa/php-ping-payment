<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class PingPayment
{
	private static $products;
	private static $flow;

	private static function selfConstruct()
	{
		$finpaa = new Finpaa();
		self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Ping_Pelvo()->Products();
		self::$flow = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Ping_Pelvo()->Products()->flow();
	}

	private static function getSequenceCode($name = 'flow')
	{
		if(self::$$name) {
			return self::$$name;
		}
		else {
			self::selfConstruct();
			return self::getSequenceCode($name);
		}
	}

	private static function executeMethod($methodCode, $alterations, $returnPayload = false)
	{
		$response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
		return array('error' => false, 'response' => json_decode(json_encode($response), true));
	}

	static function createMerchant($orgName, $orgNo, $country)
	{
		$methodCode = self::getSequenceCode()->createMerchant();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET'),
			'Obey-Rules' => "false"
		);

		$alterations[]['body'] = array(
			'name' => $orgName,
			'organization' => array(
				'se_organization_number' => $orgNo,
			    'country' => $country
			),
		);

		return self::executeMethod($methodCode, $alterations);
	}

	static function createPaymentOrder($currency)
	{
		$methodCode = self::getSequenceCode()->createPaymentOrder();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);

		$alterations[]['body'] = array(
			'currency' => $currency
		);

		return self::executeMethod($methodCode, $alterations);
	}

	static function createPingDeposit($paymentOrderId, $body)
	{
		$methodCode = self::getSequenceCode()->createPingDeposit();

		$alterations[]['path']['payment_order_id'] = $paymentOrderId;
		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);
		$alterations[]['body'] = $body;

		return self::executeMethod($methodCode, $alterations);
	}

	static function getPaymentStatus($paymentOrderId, $paymentId)
	{
		$methodCode = self::getSequenceCode('products')->payments()->getPayment();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);

		$alterations[]['path'] = array(
			'payment_order_id' => $paymentOrderId,
			'payment_id' => $paymentId
		);

		return self::executeMethod($methodCode, $alterations);
	}

	static function splitPaymentOrder($paymentOrderId)
	{
		$methodCode = self::getSequenceCode('products')->paymentOrder()->splitPaymentOrder();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);

		$alterations[]['path'] = array(
			'payment_order_id' => $paymentOrderId
		);

		$alterations[]['body'] = array(
			'fast_forward' => true
		);

		return self::executeMethod($methodCode, $alterations);
	}

	static function settlePaymentOrder($paymentOrderId)
	{
		$methodCode = self::getSequenceCode('products')->paymentOrder()->settlePaymentOrder();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);

		$alterations[]['path'] = array(
			'payment_order_id' => $paymentOrderId
		);

		$alterations[]['body'] = array(
			'fast_forward' => true
		);

		return self::executeMethod($methodCode, $alterations);
	}

	static function getPaymentOrders()
	{
		$methodCode = self::getSequenceCode('products')->paymentOrder()->getPaymentOrders();

		$alterations[]['headers'] = array(
			'tenant_id' => env('PING_TENANT_ID'),
			'x-api-secret' => env('PING_API_SECRET')
		);
		
		return self::executeMethod($methodCode, $alterations);
	}
}
